# Idem Docker Image

**saltstack/idem** is a Docker container to run idem.

## Usage

Running the application with `--help` shows all available options

```
docker run saltstack/idem --help
```

## Installing packages

Run the container with the "pip" subcommand to add plugins to the environment

```
docker run saltstack/idem pip install idem-aws ...
```

Commit a new container that has all the idem plugins you want from pip

```
docker commit $(docker ps -lq) idem:my_idem
```

Run idem!

```
docker run idem:my_idem
```

## License

Apache 2.0
