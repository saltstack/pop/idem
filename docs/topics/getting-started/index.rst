===============
Getting Started
===============

.. toctree::
   :maxdepth: 1
   :glob:

   Getting Started with Idem <https://docs.idemproject.io/getting-started/>
   Quickstart with Idem-AWS <https://docs.idemproject.io/idem-aws/en/latest/quickstart/>
