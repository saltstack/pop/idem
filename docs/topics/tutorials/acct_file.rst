========================
Account Credentials File
========================

In Idem, you can supply credentials for many different environments.
Credentials are stored in a single encrypted file.
An account credentials file follows this pattern:

.. code-block:: yaml

    credential_provider_1:
        profile_1:
            key_1: value_1
            key_2: value_2
        profile_2:
            key_1: value_1
            key_2: value_2

    credential_provider_2.acct_sub:
        profile_1:
            key_1: value_1
            key_2: value_2
        profile_2:
            key_1: value_1
            key_2: value_2

Providers
=========

In the account file, the first level of keys are the ``provider`` keys.
For every system that uses ``ACCT`` for authentication, there's a top-level Python file that specifies the provider keys that are acceptable for authenticating to that system.
The general format for the Python code follows this pattern:

.. code-block:: python

    def __init__(hub):
        hub.my_dyne.my_subsystem.ACCT = ["my_provider"]

The code above enables the ``my_provider`` provider keys to authenticate ``my_dyne.my_subsystem``.
``my_dyne`` could be ``exec``, ``states``, ``tool``, ``evbus``, ``esm``, ``sources``, or another dynamic namespace.
``my_subsystem`` is the root folder name of your cloud-specific code under the dynamic namespace.

ACCT Plugins
============

Provider keys can specify an account plugin that performs additional processing for a profile.
In the following example, the `aws.gsuite <https://gitlab.com/vmware/idem/idem-aws/-/blob/master/idem_aws/acct/aws/gsuite.py>`_
account plugin uses a Google username and password to obtain valid tokens and keys for idem-aws.

.. code-block:: yaml

    aws.gsuite:
        my_profile:
           username: my_google_user
           password: my_google_password

Profiles
========

The second level of keys in the account file are the ``profiles`` under each provider.

The default profile is usually named ``default`` if no other profile is named.
The ``default`` name is only an optional convention, not a requirement. Some components, like :doc:`evbus <../../developer/reference/events>`, don't use default profiles.

You can add multiple profiles under a provider, where each profile under the same provider has a unique name.
Duplicate profile names must be under different providers.
For example, a ``default`` AWS profile and ``default`` Azure profile are acceptable.

Profile names must match regex '[-.\w]+'; for ASCII text, this includes a-z, A-Z, 0-9, _ . and -.

.. code-block:: yaml

    aws:
      default:
        id: XXXXXXXXXXXXXXXXX
        key: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        region: us-east-1
    azure:
      default:
        client_id: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
        secret: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        subscription_id: "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
        tenant: "cccccccc-cccc-cccc-cccc-cccccccccccc"

Each system specifies and uses profiles differently.
See the :doc:`evbus <../../developer/reference/events>`, :doc:`state <../reference/sls_acct>`, :doc:`sources <../reference/sls_sources>`, and :doc:`esm <../explanation/enforced_state_management>` documentation for details on how each specifies and uses profiles.

Backends
========

External credential stores can also contain account profile information.
In the account file, these are specified under the ``acct-backend`` top-level key:

.. code-block:: yaml

    acct-backend:
      lastpass:
          username: user@example.com
          password: password
          designator: acct-provider
      keybase:
          username: user
          password: password

Extras
======

Some plugins make use of non-secret values in their authentication methods.
These are specified in the Idem config file under ``acct.extras``.

.. code-block:: yaml

    acct:
      extras:
        my_provider:
          my_profile:
            my_key: my_non_secret_value

In code, you access extras via ``hub.OPT`` as shown in the following example:

.. code-block:: python

    def gather(hub, provider: str, profile: str) -> dict:
        return hub.OPT.acct.extras[provider][profile]
