=============
"Exec" States
=============

Exec modules can be run from SLS using the ``exec.run`` state.

The return from the exec module is put in the state's *new_state*, so it can be used in arg_binding.
The first comment in the exec module state return is the cli command that can be used to call the exec module.

The ``path`` is the reference path to the exec module.
An exec module named ``func`` in the directory ``project_root/project/exec/exec_sub/plugin.py`` has a ``path`` of ``exec.exec_sub.plugin.func``.
It can be accessed on the hub via ``hub.exec.exec_sub.plugin.func()``.
The ``path`` that should be passed to the ``exec.run`` state would be ``exec_sub.plugin.func``
The acct_profile will be used to create a ctx based on the appropriate provider for the given ``path``.

.. code-block:: yaml

    exec_func:
      exec.run:
        - path: test.more
        - acct_profile: default
        - kwargs:
            kwarg_1: val_1
            kwarg_2: val_2
            kwarg_3: val_3

Output of running the state with ``--output=json``:

.. code-block:: json

    {
    "exec_|-exec_func_|-exec_func_|-run": {
        "tag": "exec_|-exec_func_|-exec_func_|-run",
        "name": "exec_func",
        "changes": {},
        "new_state": {
            "args": [
                "arg1",
                "arg2",
                "arg3"
            ],
            "kwargs": {
                "kwarg_1": "val_1",
                "kwarg_2": "val_2",
                "kwarg_3": "val_3"
            },
            "ctx": {
                "acct": {}
            }
        },
        "old_state": {},
        "comment": [
            "idem exec test.more --acct-profile=default arg1 arg2 arg3 kwarg_1=val_1 kwarg_2=val_2 kwarg_3=val_3"
        ],
        "result": true,
        "esm_tag": "exec_|-exec_func_|-exec_func_|-",
        "__run_num": 1,
        "start_time": "2022-05-31 15:08:13.548936",
        "total_seconds": 0.001457
        }
    }
