=================
Delayed Rendering
=================

By default, the states in Idem SLS files are rendered, compiled, and executed all at once. If you need to separate them, Idem supports a dependency delay between states.

To render states separately, add the following line before the state that you need to delay.

.. code-block:: sls

    #!require:<prerequisite_state>

In the following example, State_B isn't rendered until State_A has been rendered:

.. code-block:: sls

    State_A:
      test.nop
    #!require: State_A
    State_B:
      test.nop

Closing a Delayed State Block
=============================

Expanding on the preceding example, if you have State_C that's safe to run in parallel with State_A, you can close the delayed State_B block:

.. code-block:: sls

    State_A:
      test.nop
    #!require: State_A
    State_B:
      test.nop
    #!END
    State_C:
      test.nop

Otherwise, both State_B and State_C render after State_A.

If no ``#!END`` is provided, all blocks close at the end of the SLS file.
