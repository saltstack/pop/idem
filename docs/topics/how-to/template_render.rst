========================
Template Render Function
========================

Idem supports rendering data from jinja template file or an embedded jinja template.
The 'variables' includes key-value pairs which will be used for interpolation within the template.
The function can accept a Jinja template or a template file.

For example:

.. code-block:: yaml

    my-rendered-data:
      template.render:
        - template: '{% raw %}Hello {{ name }} !!{% endraw %}'
        - variables:
            name: "World"


The preceding render function produces the below content:

.. code-block:: text

    Hello World !!
