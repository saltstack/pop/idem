===========
Tool Plugin
===========

A Tool plugin contains functions that you wish to keep internal to your
Provider Plugin and not expose to the command line, such as a function to
modify the shape of a datastructure returned from your cloud API.
