======
Idem 6
======

With great pleasure we are excited to release Idem 6! This release includes
a number of major feature enhancements that make the underlying platform and
language significantly more powerful.

Mod System
==========

The mod system allows for last minute injection to execute which can modify
the data being executed right before it is run. This also comes with the
mod_aggregate system, which allows state modules to implement a function
in a state plugin called `mod_aggregate` which can be used to modify the
execution right before it is called.

Listen
======

The new `Listen` requisite allows for modifications to a state to be executed
after the entire run completes. This requisite is useful for those who want
to be able to react to changes in states without modifying the order of
execution.

Any and All Requisites
======================

A new system is now in place that allows for requisites to be triggered based
either all requisites being met, or just some of the requisites being met. The
new plugin subsystem allows for deeply dynamic handling of this intersection
in the evaluation of execution.
