def test_true(hub):
    ret = hub.exec.configurable.resource(comment=True)
    assert ret.comment == [True]


def test_false(hub):
    ret = hub.exec.configurable.resource(comment=False)
    assert ret.comment == [False]


def test_none(hub):
    ret = hub.exec.configurable.resource(comment=None)
    assert ret.comment == []


def test_int(hub):
    ret = hub.exec.configurable.resource(comment=0)
    assert ret.comment == [0]


def test_string(hub):
    ret = hub.exec.configurable.resource(comment="str")
    assert ret.comment == ["str"]


def test_bytes(hub):
    ret = hub.exec.configurable.resource(comment=b"bytes")
    assert ret.comment == [b"bytes"]


def test_tuple(hub):
    ret = hub.exec.configurable.resource(comment=("tuple",))
    assert ret.comment == ["tuple"]


def test_set(hub):
    ret = hub.exec.configurable.resource(
        comment={
            "set",
        }
    )
    assert ret.comment == ["set"]


def test_dict(hub):
    ret = hub.exec.configurable.resource(comment={"k": "v"})
    assert ret.comment == [{"k": "v"}]


def test_flatten(hub):
    ret = hub.exec.configurable.resource(
        comment=["a", "b", ["c", ["d", ["e"], "f"], "g"], "h", "i", [[[["j"]]]]]
    )
    assert ret.comment == ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]


def test_empty(hub):
    # Flattening the comments should remove empty structures
    ret = hub.exec.configurable.resource(
        comment=[set(), dict(), tuple(), [], 0, False, None, [], "", b""]
    )
    assert ret.comment == [0, False]
