def test_exec_doc(tree, idem_cli):
    ref = "exec.test.ping"
    ret = idem_cli("doc", ref, "--output=json", check=True)
    data = ret.json
    assert data
    assert ref in data
    assert data[ref]["doc"] == "Immediately return success"
    assert data[ref]["file"].endswith("test.py"), data[ref]["file"]
    assert data[ref]["ref"] == ref
    assert "parameters" in data[ref]
    assert "contracts" in data[ref]
    assert "start_line_number" in data[ref]
    assert "end_line_number" in data[ref]


def test_state_doc(tree, idem_cli):
    ref = "states.test.present"
    ret = idem_cli("doc", ref, check=True)
    data = ret.json

    assert (
        data[ref]["doc"]
        == "Return the previous old_state, if it's not specified in sls, and the given new_state.\nRaise an error on fail"
    )
    assert data[ref]["file"].endswith("test.py"), data[ref]["file"]
    assert data[ref]["ref"] == ref
    assert "parameters" in data[ref]
    assert "contracts" in data[ref]
    assert "start_line_number" in data[ref]
    assert "end_line_number" in data[ref]


def test_full_doc(tree, idem_cli):
    data = idem_cli("doc", check=True)

    # We're not going to verify the documentation of every single function on the hub
    # It's enough that the command was successful and returned more than one thing
    assert len(data) > 1


def test_list_contract_subs(idem_cli):
    """
    Verify that we have the ability to list contracts with 'idem doc'
    """
    idem_cli("doc", "tool._contract_subs", check=True)


def test_get_contract_sub_from_list(idem_cli):
    """
    Verify that we have the ability  access a specific contract in the list with dot ref syntax
    """
    idem_cli("doc", "tool._contract_subs.0", check=True)


def test_get_contract_sub_from_list_bracket(idem_cli):
    """
    Verify that we have the ability  access a specific contract in the list with bracket syntax
    """
    idem_cli("doc", "tool._contract_subs[0]", check=True)


def test_list_recursive_contract_subs(idem_cli):
    """
    Verify that we have the ability to list contracts with 'idem doc'
    """
    idem_cli("doc", "tool._recursive_contract_subs", check=True)


def test_get_recursive_contract_sub_from_list(idem_cli):
    """
    Verify that we have the ability  access a specific contract in the list with dot ref syntax
    """
    idem_cli("doc", "tool._recursive_contract_subs.0", check=True)


def test_get_recursive_contract_sub_from_list_bracket(idem_cli):
    """
    Verify that we have the ability  access a specific contract in the list with bracket syntax
    """
    idem_cli("doc", "tool._recursive_contract_subs[0]", check=True)


def test_doc_example(idem_cli):
    """
    Verify that idem doc lists parameters
    """
    ret = idem_cli("doc", "tool.choice.my_function", "--output=json")
    assert ret.json["tool.choice.my_function"]["parameters"]["param1"]["choices"] == [
        "choice1",
        "choice2",
    ]
    assert (
        ret.json["tool.choice.my_function"]["parameters"]["param1"]["annotation"]
        == "<class 'str'>"
    )
    assert ret.json["tool.choice.my_function"]["parameters"]["param2"]["choices"] == [
        1,
        2,
    ]
    assert (
        ret.json["tool.choice.my_function"]["parameters"]["param2"]["annotation"]
        == "<enum 'Choice'>"
    )
