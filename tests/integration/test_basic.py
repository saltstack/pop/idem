import pop.hub
import pytest
import rend
from pytest_idem import runner
from pytest_idem.runner import idem_cli
from pytest_idem.runner import IdemRunException
from pytest_idem.runner import run_sls
from pytest_idem.runner import run_yaml_block


def test_meta():
    ret = run_sls(["meta"], ret_data="all")
    assert "meta" in ret
    assert "sls_refs" in ret
    sls_ref = next(iter(ret["sls_refs"].keys()))
    assert ret["meta"]["SLS"][sls_ref]["foo"] == "Bar"
    assert ret["meta"]["SLS"][sls_ref]["baz"] == ["one", 1, True]
    assert ret["meta"]["SLS"][sls_ref]["bar"] == {"another": 4}
    assert ret["meta"]["ID_DECS"][f"{sls_ref}.happy"]["deep"] == "id_space"


def test_treq():
    ret = run_sls(["treq"])
    assert ret["test_|-to_treq_|-to_treq_|-treq"]["__run_num"] == 4


def test_treq_merging(tmpdir, tests_dir):
    test_sls = """
    test_resource:
        test_resource.present:
          - new_state: {resource_id: resource123}

    treq_resource:
        test.present:
          - new_state: {resource_id: resource123}

    """

    acct_data = {"profiles": {"test": {"default": {}}}}
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(test_sls)
        ret = idem_cli(
            "state",
            fh,
            "--esm-plugin=null",
            "--run-name=test",
            f"--test",
            check=True,
            acct_data=acct_data,
        ).json

        assert ret["test_|-treq_resource_|-treq_resource_|-present"]["__run_num"] == 1
        assert (
            ret["test_resource_|-test_resource_|-test_resource_|-present"]["__run_num"]
            == 2
        )


def test_ugly1():
    with pytest.raises(IdemRunException) as e:
        run_sls(["ugly1"])
        assert "ID foo in SLS ugly1 is not a dictionary" == str(e)


def test_shebang():
    ret = run_sls(["bang"])
    assert "test_|-test_|-test_|-nop" in ret


def test_req_chain():
    """
    Test that you can chain requisites, bug #11
    """
    ret = run_sls(["recreq"])
    assert ret.get("test_|-first thing_|-first thing_|-nop", {}).get("__run_num") == 1
    assert ret.get("test_|-second thing_|-second thing_|-nop", {}).get("__run_num") == 2
    assert ret.get("test_|-third thing_|-third thing_|-nop", {}).get("__run_num") == 3


def test_nest(tests_dir):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.add("tests.nest")
    hub.pop.sub.load_subdirs(hub.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest.again)
    ret = run_sls(["nest"], hub=hub, acct_file=tests_dir / "files" / "acct")
    assert ret["nest.again.another.test_|-baz_|-baz_|-nop"]["result"]
    assert ret["nest.again.test_|-bar_|-bar_|-nop"]["result"]
    assert ret["nest.test_|-foo_|-foo_|-nop"]["result"]
    # verify that the invalid state is not run
    assert not ret["idem.init_|-quo_|-quo_|-create"]["result"]
    assert ret["test_|-req_|-req_|-nop"]["__run_num"] == 5


def test_basic():
    """
    Test the basic functionality of Idem
    """
    ret = run_sls(["simple"])
    assert ret["test_|-happy_|-happy_|-nop"]["result"] is True
    assert ret["test_|-happy_|-happy_|-nop"]["changes"] == {}
    assert ret["test_|-happy_|-happy_|-nop"]["name"] == "happy"
    assert ret["test_|-sad_|-sad_|-fail_without_changes"]["result"] is False
    assert ret["test_|-sad_|-sad_|-fail_without_changes"]["name"] == "sad"
    assert ret["test_|-sad_|-sad_|-fail_without_changes"]["changes"] == {}


def test_any():
    """
    Test the require_any functionality of Idem
    """
    ret = run_sls(["any"])
    assert ret["test_|-bad_|-bad_|-nop"]["result"] is False
    assert ret["test_|-runs_|-runs_|-nop"]["result"] is True


def test_req():
    """
    Test basic requisites
    """
    ret = run_sls(["req"])
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["result"] is False
    assert ret["test_|-needs_in_|-needs_in_|-nop"]["__run_num"] == 1
    assert ret["test_|-fails_|-fails_|-fail_without_changes"]["__run_num"] == 2
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["__run_num"] == 3
    assert ret["test_|-needed_|-needed_|-nop"]["__run_num"] == 4
    assert ret["test_|-needs_|-needs_|-nop"]["__run_num"] == 5
    assert ret["test_|-needs_|-needs_|-nop"]["result"] is True


def test_unique_1():
    """
    Test unique requisite
    """
    ret = run_sls(["unique_1"])
    # Verify all the runs are successful and ordered as expected
    assert ret["test_|-ind-1_|-ind-1_|-unique_op"]["result"] is True
    assert ret["test_|-ind-1_|-ind-1_|-unique_op"]["__run_num"] == 1
    assert ret["test_|-dep-1_|-dep-1_|-unique_op"]["result"] is True
    assert ret["test_|-dep-1_|-dep-1_|-unique_op"]["__run_num"] == 2
    assert ret["test_|-dep-2_|-dep-2_|-unique_op"]["result"] is True
    assert ret["test_|-dep-2_|-dep-2_|-unique_op"]["__run_num"] == 3
    assert ret["test_|-dep-3_|-dep-3_|-unique_op"]["result"] is True
    assert ret["test_|-dep-3_|-dep-3_|-unique_op"]["__run_num"] == 4
    assert ret["test_|-ind-2_|-ind-2_|-unique_op"]["result"] is True
    assert ret["test_|-ind-2_|-ind-2_|-unique_op"]["__run_num"] == 5


def test_unique_2():
    """
    Test unique requisite
    With multiple dependencies
    """
    ret = run_sls(["unique_2"])
    assert ret["test_|-ind-1_|-ind-1_|-unique_op"]["result"] is True
    assert ret["test_|-ind-1_|-ind-1_|-unique_op"]["__run_num"] == 1
    assert ret["test_|-ind-3_|-ind-3_|-nop"]["result"] is True
    assert ret["test_|-ind-3_|-ind-3_|-nop"]["__run_num"] == 2
    assert ret["test_|-ind-2_|-ind-2_|-unique_op"]["result"] is True
    assert ret["test_|-ind-2_|-ind-2_|-unique_op"]["__run_num"] == 3
    assert ret["test_|-dep-1_|-dep-1_|-unique_op"]["result"] is True
    assert ret["test_|-dep-1_|-dep-1_|-unique_op"]["__run_num"] == 4
    assert ret["test_|-dep-2_|-dep-2_|-unique_op"]["result"] is True
    assert ret["test_|-dep-2_|-dep-2_|-unique_op"]["__run_num"] == 5
    assert ret["test_|-dep-3_|-dep-3_|-unique_op"]["result"] is True
    assert ret["test_|-dep-3_|-dep-3_|-unique_op"]["__run_num"] == 6


def test_unique_3():
    """
    Test unique requisite
    With dependencies tree
    """
    ret = run_sls(["unique_3"])
    assert ret["test_|-nop-3_|-nop-3_|-nop"]["result"] is True
    assert ret["test_|-nop-3_|-nop-3_|-nop"]["__run_num"] == 1
    assert ret["test_|-dep-3_|-dep-3_|-unique_op"]["result"] is True
    assert ret["test_|-dep-3_|-dep-3_|-unique_op"]["__run_num"] == 2
    assert ret["test_|-nop-2_|-nop-2_|-nop"]["result"] is True
    assert ret["test_|-nop-2_|-nop-2_|-nop"]["__run_num"] == 3
    assert ret["test_|-dep-2_|-dep-2_|-unique_op"]["result"] is True
    assert ret["test_|-dep-2_|-dep-2_|-unique_op"]["__run_num"] == 4
    assert ret["test_|-nop-1_|-nop-1_|-nop"]["result"] is True
    assert ret["test_|-nop-1_|-nop-1_|-nop"]["__run_num"] == 5
    assert ret["test_|-dep-1_|-dep-1_|-unique_op"]["result"] is True
    assert ret["test_|-dep-1_|-dep-1_|-unique_op"]["__run_num"] == 6


def test_sensitive():
    """
    Test sensitive requisite
    """
    ret = run_sls(["sensitive"])
    # "old_state" and "new_state" should still contain all data but changes should only contain the non-sensitive data.
    assert {"public": "public-data-old", "secret": "secret-data-old"} == ret[
        "test_|-first_thing_|-my-resource_|-present"
    ]["old_state"]
    assert {"public": "public-data", "secret": "secret-data"} == ret[
        "test_|-first_thing_|-my-resource_|-present"
    ]["new_state"]
    assert {"public": "public-data-old"} == ret[
        "test_|-first_thing_|-my-resource_|-present"
    ]["changes"]["old"]
    assert {"public": "public-data"} == ret[
        "test_|-first_thing_|-my-resource_|-present"
    ]["changes"]["new"]

    # If the sensitive requisite contains a non-existing parameter, then there will be no-op on that parameter.
    assert {"public": "public-data-old", "secret": "secret-data-old"} == ret[
        "test_|-second_thing_|-my-resource-2_|-present"
    ]["old_state"]
    assert {"public": "public-data", "secret": "secret-data"} == ret[
        "test_|-second_thing_|-my-resource-2_|-present"
    ]["new_state"]
    assert {"public": "public-data-old", "secret": "secret-data-old"} == ret[
        "test_|-second_thing_|-my-resource-2_|-present"
    ]["changes"]["old"]
    assert {"public": "public-data", "secret": "secret-data"} == ret[
        "test_|-second_thing_|-my-resource-2_|-present"
    ]["changes"]["new"]


def test_prereq():
    """
    Test prereq
    """
    ret = run_sls(["prereq"])
    assert ret["test_|-pre_|-pre_|-nop"]["result"] is True
    assert ret["test_|-pre_|-pre_|-nop"]["__run_num"] == 2
    assert ret["test_|-good_|-good_|-nop"]["__run_num"] == 1
    assert ret["test_|-change_1_|-change_1_|-succeed_with_changes"]["__run_num"] == 3
    assert ret["test_|-change_1_|-change_1_|-succeed_with_changes"]["changes"]


def test_mod_aggregate(tests_dir):
    """
    Test prereq
    """
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.add("tests.nest")
    hub.pop.sub.load_subdirs(hub.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest.again)
    ret = run_sls(["agg"], hub=hub, acct_file=tests_dir / "files" / "acct")
    assert (
        ret["nest.agg_|-aggregate_|-aggregate_|-comment"]["comment"]
        == "Modified by mod_aggregate and the mod system"
    )


def test_req_test_mode():
    """
    Test basic requisites in test mode
    """
    ret = run_sls(["req"], test=True)
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["result"] is False
    assert ret["test_|-needs_in_|-needs_in_|-nop"]["__run_num"] == 1
    assert ret["test_|-fails_|-fails_|-fail_without_changes"]["__run_num"] == 2
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["__run_num"] == 3
    assert ret["test_|-needed_|-needed_|-nop"]["__run_num"] == 4
    assert ret["test_|-needs_|-needs_|-nop"]["__run_num"] == 5
    # "needed" returned None and needs did not fail to run
    assert ret["test_|-needs_|-needs_|-nop"]["result"] is None


def test_watch():
    """
    Test basic requisites
    """
    ret = run_sls(["watch"])
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["__run_num"] == 2
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["comment"] == "Watch ran!"
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["result"] is True
    assert ret["test_|-changes_|-changes_|-succeed_with_changes"]["result"] is True
    assert ret["test_|-changes_|-changes_|-succeed_with_changes"]["changes"]


def test_listen():
    """
    Test basic requisites
    """
    ret = run_sls(["listen"])
    assert (
        ret["test_|-listen_changes_listen_|-listen_changes_listen_|-mod_watch"][
            "__run_num"
        ]
        == 3
    )
    assert (
        ret["test_|-listen_changes_listen_|-listen_changes_listen_|-mod_watch"][
            "comment"
        ]
        == "Watch ran!"
    )
    assert (
        ret["test_|-listen_changes_listen_|-listen_changes_listen_|-mod_watch"]["name"]
        == "listen_changes"
    )


def test_onfail():
    """
    Test basic requisites
    """
    ret = run_sls(["fails"])

    assert (
        ret["test_|-throws_exception_|-throws_exception_|-fail_with_exception"][
            "result"
        ]
        is False
    )
    assert (
        ret["test_|-throws_exception_|-throws_exception_|-fail_with_exception"][
            "__run_num"
        ]
        == 1
    )
    assert (
        "ZeroDivisionError"
        in ret["test_|-throws_exception_|-throws_exception_|-fail_with_exception"][
            "comment"
        ][0]
    )

    assert ret["test_|-fails_|-fails_|-fail_without_changes"]["result"] is False
    assert ret["test_|-fails_|-fails_|-fail_without_changes"]["__run_num"] == 2

    assert ret["test_|-runs_|-runs_|-nop"]["result"] is True
    assert ret["test_|-runs_|-runs_|-nop"]["__run_num"] == 3

    assert ret["test_|-bad_|-bad_|-nop"]["result"] is False
    assert ret["test_|-bad_|-bad_|-nop"]["__run_num"] == 4


def test_onchanges():
    ret = run_sls(["changes"])
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["__run_num"] == 2
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["result"] is True


def test_run_name():
    ret = run_sls(["update"])
    assert ret["test_|-king_arthur_|-king_arthur_|-nop"]["__run_num"] == 2


def test_run_num_order():
    ret = run_sls(["order"], runtime="serial")
    assert ret["test_|-first_|-first_|-nop"]["__run_num"] == 1
    assert ret["test_|-second_|-second_|-nop"]["__run_num"] == 2
    assert ret["test_|-third_|-third_|-nop"]["__run_num"] == 3
    assert ret["test_|-forth_|-forth_|-nop"]["__run_num"] == 4
    assert ret["test_|-fifth_|-fifth_|-nop"]["__run_num"] == 5
    assert ret["test_|-sixth_|-sixth_|-nop"]["__run_num"] == 6
    assert ret["test_|-seventh_|-seventh_|-nop"]["__run_num"] == 7
    assert ret["test_|-eighth_|-eighth_|-nop"]["__run_num"] == 8
    assert ret["test_|-ninth_|-ninth_|-nop"]["__run_num"] == 9
    assert ret["test_|-tenth_|-tenth_|-nop"]["__run_num"] == 10


def test_blocks():
    ret = run_sls(
        ["blocks"],
        hard_fail_on_collect=False,
    )
    assert "test_|-wow_|-wow_|-nop" in ret.keys()
    assert ret["test_|-happy_|-happy_|-nop"]["__run_num"] == 1
    assert ret["test_|-wow_|-wow_|-nop"]["__run_num"] == 2


def test_dup_keys():
    with pytest.raises(rend.exc.RenderException) as e:
        run_sls(["dupkeys"])
        assert (
            "Error rendering sls: RenderException: Yaml render error: found conflicting ID 'key'"
            == str(e)
        )


def test_acct(code_dir):
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.add("tests.nest")
    hub.pop.sub.load_subdirs(hub.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest.again)
    acct_fn = code_dir.joinpath("tests", "files", "acct")
    ret = run_sls(["acct"], acct_file=str(acct_fn), acct_key=None, hub=hub)
    assert (
        ret["nest.acct_|-gather_acct_|-gather_acct_|-gather"]["comment"]
        == '{"foo": "bar"}'
    )
    assert (
        ret["nest.acct_|-another_acct_|-another_acct_|-gather"]["comment"]
        == '{"quo": "qux"}'
    )


def test_jinja_exec_ctx(mock_time):
    ret = run_sls(["jinctx"], acct_file=None, acct_key=None)
    assert ret == {
        "test_|-test_ctx_explicit_full_path_|-test_ctx_explicit_full_path_|-succeed_with_comment": {
            "__run_num": 3,
            "changes": {},
            "new_state": None,
            "old_state": None,
            "comment": {
                "acct": {},
                "acct_details": {},
                "test": False,
            },
            "esm_tag": "test_|-test_ctx_explicit_full_path_|-test_ctx_explicit_full_path_|-",
            "name": "test_ctx_explicit_full_path",
            "rerun_data": None,
            "result": True,
            "sls_meta": {"ID_DECS": {}, "SLS": {}},
            "start_time": str(mock_time),
            "__id__": "test_ctx_explicit_full_path",
            "tag": "test_|-test_ctx_explicit_full_path_|-test_ctx_explicit_full_path_|-succeed_with_comment",
            "total_seconds": 0.0,
            "ref": "states.test.succeed_with_comment",
            "acct_details": {},
        },
        "test_|-test_ctx_minimal_|-test_ctx_minimal_|-succeed_with_comment": {
            "__run_num": 2,
            "changes": {},
            "new_state": None,
            "old_state": None,
            "comment": {
                "acct": {},
                "acct_details": {},
                "test": False,
            },
            "esm_tag": "test_|-test_ctx_minimal_|-test_ctx_minimal_|-",
            "name": "test_ctx_minimal",
            "rerun_data": None,
            "result": True,
            "sls_meta": {"ID_DECS": {}, "SLS": {}},
            "start_time": str(mock_time),
            "__id__": "test_ctx_minimal",
            "tag": "test_|-test_ctx_minimal_|-test_ctx_minimal_|-succeed_with_comment",
            "total_seconds": 0.0,
            "ref": "states.test.succeed_with_comment",
            "acct_details": {},
        },
        "test_|-test_implicit_ctx_|-test_implicit_ctx_|-succeed_with_comment": {
            "__run_num": 1,
            "changes": {},
            "new_state": None,
            "old_state": None,
            "comment": {
                "acct": {},
                "acct_details": {},
                "test": False,
            },
            "esm_tag": "test_|-test_implicit_ctx_|-test_implicit_ctx_|-",
            "name": "test_implicit_ctx",
            "rerun_data": None,
            "result": True,
            "sls_meta": {"ID_DECS": {}, "SLS": {}},
            "start_time": str(mock_time),
            "__id__": "test_implicit_ctx",
            "tag": "test_|-test_implicit_ctx_|-test_implicit_ctx_|-succeed_with_comment",
            "total_seconds": 0.0,
            "ref": "states.test.succeed_with_comment",
            "acct_details": {},
        },
        "test_|-test_minimal_ctx_|-test_minimal_ctx_|-succeed_with_comment": {
            "__run_num": 4,
            "changes": {},
            "new_state": None,
            "old_state": None,
            "comment": {"acct": {}, "acct_details": {}, "test": False},
            "esm_tag": "test_|-test_minimal_ctx_|-test_minimal_ctx_|-",
            "name": "test_minimal_ctx",
            "rerun_data": None,
            "result": True,
            "sls_meta": {"ID_DECS": {}, "SLS": {}},
            "start_time": str(mock_time),
            "__id__": "test_minimal_ctx",
            "tag": "test_|-test_minimal_ctx_|-test_minimal_ctx_|-succeed_with_comment",
            "total_seconds": 0.0,
            "ref": "states.test.succeed_with_comment",
            "acct_details": {},
        },
    }


YAML_TEST = """
always-passes-with-any-kwarg:
  test.nop:
    - name: foo
    - something: else
    - foo: bar
"""


def test_yaml(mock_time):
    ret = run_yaml_block(YAML_TEST)
    assert ret == {
        "test_|-always-passes-with-any-kwarg_|-always-passes-with-any-kwarg_|-nop": {
            "__run_num": 1,
            "changes": {},
            "new_state": None,
            "old_state": None,
            "comment": "Success!",
            "esm_tag": "test_|-always-passes-with-any-kwarg_|-always-passes-with-any-kwarg_|-",
            "name": "foo",
            "rerun_data": None,
            "result": True,
            "sls_meta": {"ID_DECS": {}, "SLS": {}},
            "start_time": str(mock_time),
            "__id__": "always-passes-with-any-kwarg",
            "tag": "test_|-always-passes-with-any-kwarg_|-always-passes-with-any-kwarg_|-nop",
            "total_seconds": 0.0,
            "ref": "states.test.nop",
            "acct_details": {},
        }
    }


def test_missing_requisite():
    """
    Test missing requisite errors are propagated to user
    """
    ret = run_sls(["requisite_resolve_error"])
    assert ret["test_|-missing_requisite_|-missing_requisite_|-nop"]["result"] is False
    assert (
        ret["test_|-missing_requisite_|-missing_requisite_|-nop"]["comment"]
        == "Requisite require test:missing not found in ESM."
    )


def test_parallel_state():
    ret = run_sls(["parallel_state"])
    assert (
        ret["test_|-parallel-first_|-parallel-first_|-parallel_state"]["new_state"]
        .get("acct")
        .get("name")
        == "parallel-first"
    )
    assert (
        ret["test_|-parallel-second_|-parallel-second_|-parallel_state"]["new_state"]
        .get("acct")
        .get("name")
        == "parallel-second"
    )


def test_circular_dependency():
    try:
        run_sls(["req_circular"])
    except Exception as e:
        assert "No sequence changed" in str(e), str(e)
        assert "Check for possible circular dependencies" in str(e), str(e)
        return

    assert False, "test_circular_dependency: exception expected"


def test_invalid_require_state():
    # resource with SKIP_ESM and invalid arg_bind and require format
    ret = run_sls(["reqs_invalid"])
    assert ret
    tag_failure = "test_resource_|-test_2_invalid_require_format_|-test_2_invalid_require_format_|-present"
    assert ret[tag_failure]["result"] is False
    assert (
        ret[tag_failure]["comment"]
        == "Requisite 'require test_resource.present:test_1' not found in current run. Verify the syntax."
    ), ret[tag_failure]["comment"]


def test_invalid_require_missing_state():
    # resource with invalid require format
    ret = run_sls(["require_invalid"])
    assert ret
    tag_failure = "time_|-sleep_2_|-sleep_2_|-sleep"
    assert ret[tag_failure]["result"] is False
    assert ret[tag_failure]["comment"] == "sleep_1 should be dictionary", ret[
        tag_failure
    ].comment


def test_invalid_arg_bind():
    # resource with SKIP_ESM and invalid arg_bind and require format
    ret = run_sls(["arg_bind_invalid"])
    assert ret
    tag_failure = "test_resource_|-test_3_invalid_arg_bind_format_|-test_3_invalid_arg_bind_format_|-present"
    assert ret[tag_failure]["result"] is False
    assert (
        ret[tag_failure]["comment"]
        == "Requisite 'arg_bind test_resource.present:test_1' not found in current run. Verify the syntax."
    ), ret[tag_failure]["comment"]
