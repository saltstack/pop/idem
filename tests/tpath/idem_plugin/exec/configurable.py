def resource(hub, result=True, comment=None, ret=None, **kwargs):
    return dict(result=result, comment=comment, ret=ret, **kwargs)
