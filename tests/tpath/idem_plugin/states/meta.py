def present(hub, ctx, name: str):
    result = dict(result=True, old_state=None, new_state=None, comment=[], name=name)
    try:
        result["old_state"] = ctx.sls_meta
        result["new_state"] = ctx.meta
    except Exception as e:
        result["result"] = False
        result["comment"].append(f"{e.__class__.__name__}: {e}")
    return result
