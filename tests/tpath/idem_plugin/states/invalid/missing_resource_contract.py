def present(hub, ctx, name: str, required):
    ...


def absent(hub, ctx, name: str, required):
    ...


async def describe(hub, ctx):
    return {}
