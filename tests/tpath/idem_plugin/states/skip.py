__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    *,
    skip: bool,
):
    ret = {
        "name": name,
        "old_state": None,
        "new_state": None,
        "result": True,
        "comment": None,
    }
    return ret


def absent(
    hub,
    ctx,
    name: str,
    *,
    skip: bool,
):
    return {
        "name": name,
        "old_state": None,
        "new_state": None,
        "result": True,
        "comment": None,
    }


async def describe(hub, ctx):
    return {"test": {"skip.present": []}}
