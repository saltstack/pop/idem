import enum
from typing import Literal


class Choices(enum.Enum):
    A = "a"
    B = "b"
    C = "c"


def choice_enum(hub, option: Choices):
    ...


def choice_literal(hub, option: Literal["a", "b", "c"]):
    ...


def function1(hub, param1: str, param2: int):
    pass


class Choice(enum.Enum):
    A = 1
    B = 2


def my_function(hub, param1: Literal["choice1", "choice2"], param2: Choice):
    ...
