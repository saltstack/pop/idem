import pytest


@pytest.mark.parametrize(
    # fmt: off
    "source, protocol_plugin, protocol, acct_profile, data",
    [
        ["git+https://github.com/my_user/my_project.git",
         "git", "git+https", None, "github.com/my_user/my_project.git"],
        ["git+http://profile@github.com",
         "git", "git+http", "profile", "github.com"],
        ["git+http://profile.with_all-separators@github.com",
         "git", "git+http", "profile.with_all-separators", "github.com"],
        ['json://{"test.sls": {"dummy-state":{"test.succeed_with_comment":[{"email":"abc@xyz.com"}]}}}',
         "json", "json", None, '{"test.sls": {"dummy-state":{"test.succeed_with_comment":[{"email":"abc@xyz.com"}]}}}'],
        ['json://prof@{"test.sls": {"dummy-state":{"test.succeed_with_comment":[{"email":"abc@xyz.com"}]}}}',
         "json", "json", "prof", '{"test.sls": {"dummy-state":{"test.succeed_with_comment":[{"email":"abc@xyz.com"}]}}}'],
    ]
    # fmt: on
)
def test_parse_source(
    hub, mock_hub, source, protocol, protocol_plugin, acct_profile, data
):
    mock_hub.idem.get.parse_source = hub.idem.get.parse_source
    ret = mock_hub.idem.get.parse_source(source)
    assert ret[0] == protocol
    assert ret[1] == protocol_plugin
    assert ret[2] == acct_profile
    assert ret[3] == data
